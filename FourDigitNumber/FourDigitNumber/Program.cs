﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FourDigitNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter four-digit number");
            int number = 0;

            try
            {
                number = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Wrong format. Please enter a four-digit number");
            }

            int firstDigit = number / 1000;

            if (firstDigit % 2 == 0)
            {
                Console.WriteLine("First digit is an even number");
            }
            else
            {
                Console.WriteLine("First digit is an odd number");
            }
            Console.ReadLine();

        }
    }
}
