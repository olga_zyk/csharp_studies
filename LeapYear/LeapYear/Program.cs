﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYear
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the year you want to check: ");
            string year = Console.ReadLine();
            int y = Convert.ToInt32(year);

            if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
            {
                Console.WriteLine(y + " is a leap year");
            }
            else
            {
                Console.WriteLine(y + " is not a leap year");
            }

            Console.ReadLine();

            Console.WriteLine("Enter the year of your birth");
            string birth = Console.ReadLine();
            int b = Convert.ToInt32(birth);
            int army = y - b;
            if (army > 0 && army > 18 && army < 27)
            {
                Console.WriteLine("In the " + y + " year you was " + army + " years old and if you are a man you would join the army");
            }
            else if (army > 0 && army < 18)
            {
                Console.WriteLine("In the " + y + " year you was " + army + " years old and you wouldn't join the army");
            }
            else if (army < 0)
            {
                Console.WriteLine("You even wasn't born in the " + y +" year");
            }

            Console.ReadLine();
        }
    }
}
