﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Studies
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please, enter integer numbers separated by commas");
            string text = Console.ReadLine();
            string[] numArray = text.Split(',');
            int[] numbers = new int[numArray.Length];

            for (int i = 0; i < numArray.Length; i++)
            {
                try
                {
                    numbers[i] = int.Parse(numArray[i]);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Wrong format. Please enter numbers separated by comma");
                    continue;
                }
            }

            //the sum (using for loop)
            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sum = sum + numbers[i];
            }
            Console.WriteLine("The sum (using for loop): " + sum);

            //sum (using foreach loop)
            int sumForeach = 0;
            foreach (int number in numbers)
            {
                sumForeach = sumForeach + number;
            }
            Console.WriteLine("the sum (using foreach loop): " + sumForeach);

            //the biggest number
            int numBig = 0;
            foreach (int number in numbers)
            {
                if (number > numBig)
                {
                    numBig = number;
                }
            }
            Console.WriteLine("The biggest number: " + numBig);

            //the smallest number
            int numSmall = 1;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] < numSmall)
                {
                    numSmall = numbers[i];
                }
            }
            Console.WriteLine("The smallest number: " + numSmall);

            //using Min and Max methods
            int max = numbers.Max();
            int min = numbers.Min();
            Console.WriteLine("The biggest is " + max);
            Console.WriteLine("The smallest is " + min);


            Console.ReadLine();
        }
    }
}
