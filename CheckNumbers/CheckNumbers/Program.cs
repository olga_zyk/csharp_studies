﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number and press ENTER (to exit enter EXIT");

            do
            {
                string num = Console.ReadLine();
                int number = 0;

                if (num.Equals("exit"))
                {
                    Environment.Exit(0);
                }

                try
                {
                    number = Convert.ToInt32(num);
                }
                catch (FormatException)
                {
                    Console.WriteLine("To continue please enter a number");
                    continue;
                }

                if (number > 0)
                {
                    Console.WriteLine("You entered a positive number");
                }
                else if (number < 0)
                {
                    Console.WriteLine("You entered a negative number");
                }
                else if (number == 0)
                {
                    Console.WriteLine("You entered null");
                }

            } while (true);

        }
    }
}
