﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRenovation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the width of the room: ");
            string width = Console.ReadLine();
            double wDouble = Convert.ToDouble(width);

            Console.WriteLine("Enter the length of the room: ");
            string length = Console.ReadLine();
            double lDouble = Convert.ToDouble(length);

            Console.WriteLine("Enter the hight of the room: ");
            string height = Console.ReadLine();
            double hDouble = Convert.ToDouble(height);

            double wallsSquare = ((2 * (wDouble + lDouble)) + (2 * (hDouble + lDouble)));
            double roomSquare = (0.85 * wallsSquare);
            Console.WriteLine("the square of your room is: " + roomSquare + " m2");

            Console.WriteLine("Do you want quality or fast renovation?");
            string answer = Console.ReadLine();

            if (answer.Equals("quality"))
            {
                Console.WriteLine("Your renovation will cost: " + ((40 * roomSquare) + (2 * roomSquare) + (10 * roomSquare)) + " euro");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Your renovation will cost: " + ((20 * roomSquare) + (1 * roomSquare) + (10 * roomSquare)) + " euro");
                Console.ReadLine();

            }
        }
    }
}
