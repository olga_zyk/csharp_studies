﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iveskite pirmąji skaiciu");
            string a = Console.ReadLine();
            int aInt = Convert.ToInt32(a);

            Console.WriteLine("Iveskite antraji skaiciu");
            string b = Console.ReadLine();
            int bInt = Convert.ToInt32(b);

            Palyginti(aInt, bInt);
            Console.ReadLine();
        }

        static void Palyginti(int aInt, int bInt)
        {

            if (aInt > bInt)
            {
                Console.WriteLine("Pirmas skaicius yra daugiau nei antras");
            }
            else
            {
                Console.WriteLine("Antras skaicius yra daugiau nei pirmas");
            }

        }

    }
}

